import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

function EditRental() {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  let navigate = useNavigate();
  const { id } = useParams();

  const updateRental = async (e) => {
    e.preventDefault();
    await axios.patch(`http://localhost:3000/rental/${id}`, {
      title: title,
      author: author
    });
    navigate("/", { replace: true });
  };

  useEffect(() => {
    getRentalByID();
  }, []);

  const getRentalByID = async () => {
    const res = await axios.get(`http://localhost:3000/rental/${id}`);
    setTitle(res.data.title);
    setAuthor(res.data.author);
  };
  return (
    <>
      <div>
        <form onSubmit={updateRental}>
          <label htmlFor="">title</label>
          <input
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />

          <label htmlFor="">author</label>
          <input
            type="text"
            value={author}
            onChange={(e) => setAuthor(e.target.value)}
          />
          <button>Simpan</button>
        </form>
      </div>
    </>
  );
}

export default EditRental;
