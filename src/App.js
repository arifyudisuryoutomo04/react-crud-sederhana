import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from "./homePage";
import EditRental from "./EditRental";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/edit-rental/:id" element={<EditRental />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
