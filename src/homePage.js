import "./App.css";
import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate, useParams, Link } from "react-router-dom";

function HomePage() {
  const [rental, setRental] = useState([]);
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");

  let navigate = useNavigate();

  const getRental = async () => {
    const res = await axios.get("http://localhost:3000/rental");
    setRental(res.data);
    // console.log(res.data);
  };
  const deleteRental = async (id) => {
    await axios.delete(`http://localhost:3000/rental/${id}`);
    getRental();
  };

  const saveRental = async (e) => {
    e.preventDefault();
    await axios.post("http://localhost:3000/rental/", {
      title: title,
      author: author
    });
    navigate("/", { replace: true });
  };

  useEffect(() => {
    getRental();
  }, []);

  return (
    <>
      <div className="App">
        <div>
          <form onSubmit={saveRental}>
            <label htmlFor="">title</label>
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />

            <label htmlFor="">author</label>
            <input
              type="text"
              value={author}
              onChange={(e) => setAuthor(e.target.value)}
            />
            <button>Simpan</button>
          </form>
        </div>
        <table>
          <thead>
            <tr>
              <th>title</th>
              <th>author</th>
              <th>setting</th>
            </tr>
          </thead>
          <tbody>
            {rental.map((ren, index) => (
              <tr key={index + 1}>
                <td>{ren.title}</td>
                <td>{ren.author}</td>
                <td>
                  <button onClick={() => deleteRental(ren.id)}>delete</button>
                  <button>
                    <Link to={`/edit-rental/${ren.id}`}>edit Rental</Link>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default HomePage;
